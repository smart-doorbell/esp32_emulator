# Esp32 Emulator

Программа, которая эмулирует действия микроконтроллера Esp32, что позволяет проверить работоспособность [этого приложения](https://gitlab.com/smart-doorbell/android-app) не зависимо от микроконтроллера.

Программа делает запись данных в Firebase Realtime Database.

Программа отправляет Firebase Notifications на смартфон.

