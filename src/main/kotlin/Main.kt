import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import java.io.FileInputStream

private const val PATH_TO_PRIVATE_KEY = "/home/user576/smart-doorbell-576-firebase-adminsdk-qidyk-c4e36a1455.json"
private const val DB_URL = "https://smart-doorbell-576-default-rtdb.europe-west1.firebasedatabase.app"

private const val PHOTO_URL_1 = "https://firebasestorage.googleapis.com/v0/b/smart-doorbell-576.appspot.com/o/public%2F%D0%90%D0%BA%D1%96%D0%BC%D0%B5%D0%BD%D0%BA%D0%BE432%D0%9A%D0%91.JPG?alt=media"
private const val PHOTO_URL_2 = "https://firebasestorage.googleapis.com/v0/b/smart-doorbell-576.appspot.com/o/public%2F%D0%BE%D0%B4%D0%BD%D0%BE%D0%9B%D0%B8%D1%86%D0%BE.png?alt=media"
private val requests = arrayOf(
    DoorbellRecord(0, 1633247650650L, PHOTO_URL_1),
    DoorbellRecord(1, 1633248431620L, PHOTO_URL_2),
    DoorbellRecord(2, 1633249650650L, PHOTO_URL_1),
    DoorbellRecord(3, 1633249650750L, PHOTO_URL_2),
)

fun main() {

    val serviceAccount = FileInputStream(PATH_TO_PRIVATE_KEY)

    val options = FirebaseOptions.builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl(DB_URL)
        .build()

    FirebaseApp.initializeApp(options)

    val esp32 = Esp32()
    val iterator = requests.iterator()
    do {
        esp32.sendRequest(iterator.next())
    } while (areWeGoingNext() && iterator.hasNext())
}


private const val NEXT = "next"
private const val EXIT = "exit"
private const val PROMPT = """
Are we going next or exiting?
$NEXT or $EXIT? 
Type your choice: 
"""

fun areWeGoingNext() : Boolean {
    var answ : String?
    do {
        print(PROMPT)
        answ = readLine()
    } while (answ !in arrayOf(NEXT, EXIT))

    return answ == NEXT
}
