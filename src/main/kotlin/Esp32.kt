import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import com.google.firebase.messaging.Notification
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class Esp32 {

    private val completionListener = DatabaseReference.CompletionListener { error, lRef ->
        if (error.message != null) {
            println("error.message: ${error?.message}, ref: $lRef")
        }
    }

    private val formatter = SimpleDateFormat(FORMAT_PATTERN, Locale.getDefault())

    fun sendRequest(record : DoorbellRecord) {

        // As an admin, the app has access to read and write all data, regardless of Security Rules
        val ref = FirebaseDatabase.getInstance()
            .getReference(PHOTOS)
            .child(record.key.toString())

        ref.setValue(record, completionListener)

        // See documentation on defining a message payload.
        val message: Message = Message.builder()
            .setNotification(
                Notification.builder()
                    .setTitle(NOTIFICATION_TITLE)
                    .setBody(
                        String.format(
                            NOTIFICATION_BODY, formatter.format(Date(record.timeInstant))
                        )
                    )
                    .setImage(record.imageUrl)
                    .build()
            )
            .setToken(TOKEN)
            .build()

        FirebaseMessaging.getInstance().send(message)
    }

    companion object {
        private const val TOKEN = "e7bL6gBVSVCzy5CcvdrRVw:APA91bHq0xc9oP4GQ5Cys83wokgsOVf05XuSScpPs4C3ivOEKzXADuxlwrnnj9nfKkRnt1UNuXldajVlsJW0tpb_JSq_J8umXAQYO1dIhdBiacCd6gmwub84V7VWkYwvAvjbaDkCol65"
        private const val PHOTOS = "photos"
        private const val NOTIFICATION_TITLE = "Home access request"
        private const val NOTIFICATION_BODY = "Someone rang doorbell at %s."
        private const val FORMAT_PATTERN = "HH:mm:ss dd.MM.yyyy"
    }
}